# Objectifs de ce module

Pratique du language SQL.

# Instructions

Réalisez les requêtes SQL, A partir du modèle de base de données suivant

![Modèle logique de données](./modelisation.png)

Si vous souhaitez ajouter de la data dans la base de données afin de vérifier vos requêtes SQL, vous avez la structure SQL présente au sein de ce dépôt.

N'hésitez pas à ajouter des données afin de valider que les requêtes fonctionnent comme attendu!

# Question de base.

En important le fichier SQL `bdd.sql`, le script nous a automatiquement généré un base de données mydb. Mal nommée il serait interessant de renommer cette base de données en 'housing_demo'.

# Questions INSERT

## I1.1

Insérez un pays se nommant 'FRANCE'.

## I1.2

Insérez une ville se nommant 'Rennes'. Elle doît être rattachée à la France.

## I1.3

Insérez un utilisateur ayant le Rôle Administrateur et se nommant 'Brayan'.

# Questions UPDATE

## U1.1

L'utilisateur Brayan souhaite changer son mot de passe et définir le mot de passe 'secret'. Réalisez la requête permettant de stocker ce mot de passe en clair dans la base de données

## U1.2

Finalement, nous ne souhaitons pas que le nom du pays soit affiché en majuscule mais qu'il n'ait que la première lettre en majuscule.

## U1.3

Nous avons rajouté une encryption basique au sein de notre application, et le mot de passe de Brayan doit être stocké via une encryption MD5.

# Questions DELETE

## D1.1

Supprimer le compte utilisateur de Brayan

## D1.2

Supprimer la table OwnerComment

## D1.3

Supprimer toutes les villes qui ne sont liées à aucun logement ni à aucun propriétaire.

# Questions SELECT

## Question 1.1

Retournez l'ensemble des Maisons (Housing), uniquement le nom des maisons est attendu en retour

## Question 1.2

Afin d'améliorer les performances, nous aimerions que la requête précédente nous retourne maximum 25 enregistrements.

## Question 1.3

Retournez l'ensemble des propriétaires ayant une adresse email non définie.

## Question 1.4

Nous souhaitons rajouter une recherche rapide au sein de notre application. Permettant ainsi d'intéroger la base de données même si nous n'avons pas saisi l'intégralité du mot. Retournez l'ensemble des Villes contenant le texte 'enne'.

## Question 2.1

Après avoir fait appel à un développeur, nous avons detecté un bug.
Nous n'avons pas accès au code qu'il a produit mais uniquement à la requête SQL executée. Il faudrait modifier la requête suivante afin qu'au lieu de nous retourner un colonne nommée name, que ce soit une colonne nommée 'type_name'.

`SELECT name FROM HousingType`

## Question 3.1

Retournez l'ensemble des Maisons (Housing) situées à 'Rennes', uniquement le nom des maisons est attendu

## Question 3.2

Retournez l'ensemble des url des médias liées à la maison nommée 'Château de Moulinsart'

## Question 3.3

Retournez les statuts, ordonnés dans le temps, liés à la maison nommée 'Château de Moulinsart'. Le premier enregistrement retourné doit être le statut le plus récent.

## Question 3.4

Nous aimerions réaliser un filtre rapide permettant de retourner l'ensemble des maisons ayant plus de 3 chambres

## Question 3.5

Retournez l'ensemble des login utilisateur ayant le statut 'Administrator'.

## Question 4.1

Retournez un tableau listant, le nom de la ville ainsi que le nombre de maisons s'y trouvant. Attention, si une ville n'a aucune maison associée, elle tout de même être retournée en affichant le nombre 0.

## Question 4.2

Retournez un tableau listant le nombre de chambre maximum, minimum et moyen des maisons, regroupés par nom de commune. Les villes doivent être listées même s'il n'y a pas de maison associée.

## Question 5.1

Question
Retournez l'ensemble des maisons ayant actuellement le statut 'A vendre'.

## Question 5.2

Retournez le nombre de biens vendus, regroupés par année de vente.
